-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 15, 2018 at 11:58 AM
-- Server version: 5.7.19
-- PHP Version: 7.1.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project_managment`
--

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `id` int(11) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `student_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`id`, `user_id`, `description`, `student_id`) VALUES
(1, '2', 'This is your first task This is your first task This is your first task This is your first task This is your first task This is your first task This is your first task', '8'),
(2, '2', 'Hello Did you have submit your documentation', '7'),
(3, '7', 'Sir Please Provide Your Email Adress so Ican send you the documentation', '7'),
(4, '10', 'Hello Can you Please Assign My First Tak', '10');

-- --------------------------------------------------------

--
-- Table structure for table `instrutions`
--

CREATE TABLE `instrutions` (
  `id` int(11) NOT NULL,
  `instruction_name` varchar(255) DEFAULT NULL,
  `file_path` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `instrutions`
--

INSERT INTO `instrutions` (`id`, `instruction_name`, `file_path`) VALUES
(1, 'Project Instruction For Chemical Engineering', '5bed45928a062read me');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `name`, `description`) VALUES
(1, 'Students Notice', 'Lorem Ipsum is test text Lorem Ipsum is test text Lorem Ipsum is test text Lorem Ipsum is test text Lorem Ipsum is test text Lorem Ipsum is test text orem Ipsum is test text Lorem Ipsum is test text Lorem Ipsum is test text Lorem Ipsum is test text Lorem Ipsum is test text Lorem Ipsum is test text'),
(2, 'Teachers Notice', 'Lorem Ipsum is test text Lorem Ipsum is test text Lorem Ipsum is test text Lorem Ipsum is test text Lorem Ipsum is test text Lorem Ipsum is test text orem Ipsum is test text Lorem Ipsum is test text Lorem Ipsum is test text Lorem Ipsum is test text Lorem Ipsum is test text Lorem Ipsum is test text'),
(3, 'First Term Examination (2018-20)', 'Lorem Ipsum is test text Lorem Ipsum is test text Lorem Ipsum is test text Lorem Ipsum is test text Lorem Ipsum is test text Lorem Ipsum is test text orem Ipsum is test text Lorem Ipsum is test text Lorem Ipsum is test text Lorem Ipsum is test text Lorem Ipsum is test text Lorem Ipsum is test text'),
(4, 'Anual Charges', 'All Students Must Have to submit Anual Charges Before Final Year Project');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(11) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `reg_no` varchar(255) DEFAULT NULL,
  `program` varchar(255) DEFAULT NULL,
  `project_name` varchar(255) DEFAULT NULL,
  `file_path` varchar(255) DEFAULT NULL,
  `session` varchar(255) DEFAULT NULL,
  `smester` varchar(255) DEFAULT NULL,
  `project_status` varchar(255) DEFAULT NULL,
  `project_decription` varchar(255) NOT NULL,
  `project_scope` varchar(255) DEFAULT NULL,
  `project_domain` varchar(255) DEFAULT NULL,
  `teacher_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `user_id`, `name`, `reg_no`, `program`, `project_name`, `file_path`, `session`, `smester`, `project_status`, `project_decription`, `project_scope`, `project_domain`, `teacher_id`) VALUES
(2, '7', 'Azlan Hashmi', '78800', 'Computer Science', 'Projects Managment System', '5be5d2d31eec53.png', '2k18', '4t', 'approved', 'This project will Manage the projects of university', 'It will work all over the university', 'PHP', '2'),
(3, '8', 'Azlan Shah', '78800', 'Computer Science', 'Projects Managment System', '5be5d3b88f3f03.png', '2k18', '4t', 'pending', '', NULL, NULL, '4'),
(4, '9', 'Imran Hashmi', '78822', 'Chemical Engineering', 'Boiler Managment System', '5be5d42bd22382.jpg', '2k17', '8th', 'rejected', '', NULL, NULL, '2'),
(5, '10', 'Ahmad Hashmi', '88991010', 'BS(IT)', 'Content Managment System', '5bed555dae9afread-me', '2k16', '8th', 'approved', 'This Project Will Mange the whole Content of university', 'I will explain scope Late', 'PHP, My SQL', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `teacher_students`
--

CREATE TABLE `teacher_students` (
  `id` int(11) NOT NULL,
  `student_id` varchar(255) DEFAULT NULL,
  `teacher_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `role`, `name`) VALUES
(1, 'admin@mail.com', '1234', 'admin', 'Ahmad Raza'),
(2, 'alvi@mail.com', 'alvi1234', 'teacher', 'Ahmad Alvi'),
(3, 'sara@mail.com', 'sara123', 'admin', 'Sara Razi'),
(4, 'saraahmad@gmail.com', 'sa123', 'teacher', 'Sara Ahmad'),
(7, 'azlan@gmail.com', '12345', 'student', 'Azlan Shah'),
(8, 'azlanshah@gmail.com', '12345', 'student', 'Azlan Shah'),
(9, 'imran@gmail.com', '12345', 'student', 'Imran Hashmi'),
(10, 'shahzad@gmail.com', 'shahzad123', 'student', 'Ahmad Shahzad');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `instrutions`
--
ALTER TABLE `instrutions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher_students`
--
ALTER TABLE `teacher_students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `instrutions`
--
ALTER TABLE `instrutions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `teacher_students`
--
ALTER TABLE `teacher_students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
