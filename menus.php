
<?php

include_once('config.php');

is_login();

$user_id = $_SESSION['username'];

$current_user = get_single_record('users','id',$user_id);


?>
<aside class="left-sidebar" data-sidebarbg="skin6">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <!-- User Profile-->
                <li>
                    <!-- User Profile-->
                    <div class="user-profile d-flex no-block dropdown m-t-20">
                        <div class="user-pic"><img src="assets/images/users/1.jpg" alt="users" class="rounded-circle" width="40" /></div>
                        <div class="user-content hide-menu m-l-10">
                            <a href="javascript:void(0)" class="" id="Userdd" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <h5 class="m-b-0 user-name font-medium">

                                    <?php echo $current_user['name']; ?>

                                    <i class="fa fa-angle-down"></i></h5>
                                    <span class="op-5 user-email"><?php echo $current_user['email']; ?></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="Userdd">

                                    <a class="dropdown-item" href="logout.php"><i class="fa fa-power-off m-r-5 m-l-5"></i> Logout</a>
                                </div>
                            </div>
                        </div>
                        <!-- End User Profile-->
                    </li>

                    <!-- User Profile-->

                    <?php

                    if($current_user['role'] == 'admin'):

                       if(!empty($admin_menus)):

                        foreach ($admin_menus as $link => $admin_menu){

                            ?>
                            <li class="sidebar-item">

                                <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo $link; ?>" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu"><?php echo $admin_menu; ?></span></a></li>

                                <?php
                            }

                        endif;

                    endif;

                    if($current_user['role'] == 'student'):

                        if(!empty($student_menus)):

                            foreach ($student_menus as $link => $value) {

                                ?>
                                <li class="sidebar-item"> 

                                    <?php if($link=='student_tasks_list.php') :?>

                                        <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo $link.'?student_id='.$user_id; ?>" aria-expanded="false"><i class="mdi mdi-account-network"></i><span class="hide-menu"><?php echo $value; ?></span></a>

                                        <?php else: ?>


                                        <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo $link; ?>" aria-expanded="false"><i class="mdi mdi-account-network"></i><span class="hide-menu"><?php echo $value; ?></span></a>

                                    <?php endif; ?>

                                </li>

                                <?php
                            }

                        endif;

                    endif;

                    if($current_user['role'] == 'teacher'):

                        if(!empty($teacher_menus)):

                            foreach ($teacher_menus as $link => $teacher_menu){

                                ?>

                                <li class="sidebar-item">

                                    <?php if($teacher_menu == 'Assigned Students'): ?>

                                        <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo $link.'?teacher_id='.$user_id; ?>" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu"><?php echo $teacher_menu; ?></span></a>

                                        <?php else:?>

                                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo $link; ?>" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu"><?php echo $teacher_menu; ?></span></a>

                                        <?php endif; ?>

                                    </li>

                                    <?php
                                }

                            endif;

                        endif;

                        ?>


                    </ul>

                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>