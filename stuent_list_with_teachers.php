<!DOCTYPE html>
<html dir="ltr" lang="en">


<?php include_once('heads.php'); ?>

<?php include_once('functions.php');?>


<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full" data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
       <?php include_once('header.php'); ?>
    <!-- ============================================================== -->
    <!-- End Topbar header -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <?php

    include_once('menus.php');

    $result   = fetch_all_reord('students');

    $teachers = get_complete_reord('users','role','teacher');

    $supervisor = false;

    if (isset($_POST['submit'])) {
    $students =  $_POST['student_id'];
  $teachers =  $_POST['teacher_id'];

  if(!empty($students))
  {
    foreach ($students as $key => $students) {
        update_single_record('students','teacher_id',$teachers[$key],'id',$students);
    }
    $supervisor = true;
  }
}

    ?>
    <!-- ============================================================== -->
    <!-- End Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Page wrapper  -->
    <!-- ============================================================== -->
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="page-breadcrumb">
            <div class="row align-items-center">
                <div class="col-5">
                    <h4 class="page-title">Students List</h4>
                    <div class="d-flex align-items-center">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Library</li>
                            </ol>
                        </nav>
                    </div>
                </div>

            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Sales chart -->
            <!-- ============================================================== -->
            <div class="row">
              
               
                            <!-- ============================================================== -->
                            <!-- Table -->
                            <!-- ============================================================== -->
                            <div class="row">
                                <!-- column -->
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <!-- title -->
                                            <div class="d-md-flex align-items-center">
                                                <div>
                                                    <h4 class="card-title">Assign Super Visor To student</h4>

                                                    <?php if($supervisor) {?>

                                                    <h4 class="card-title alert alert-success"> Super Visor Assigned To student</h4>

                                                    <?php } ?>
                                                    
                                                </div>
                                                
                                            </div>
                                            <!-- title -->
                                        </div>
                                        <div class="table-responsive">
                                            <form name="registration" action="" method="post">
                                            <table class="table v-middle">
                                                <thead>
                                                    <tr class="bg-light">
                                                        <th class="border-top-0">Name</th>
                                                        <th class="border-top-0">Project</th>
                                                        <th class="border-top-0">Teacher</th>
                                                        
                                                        <th class="border-top-0">Email</th>
                                                        <th class="border-top-0">Program</th>
                                                        <th class="border-top-0">Smaster</th>
                                                        
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    while($res = mysqli_fetch_array($result)) {

                                                        ?>
                                                    <tr>
                                                        <td>
                                                            <div class="d-flex align-items-center">
                                                                
                                                                <div class="">
                                                                    <h4 class="m-b-0 font-16"><?php echo $res['name']; ?></h4>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td><?php echo $res['project_name']; ?></td>
                                                        <td>

                                                            <input type="hidden" name="student_id[]" value="<?php echo $res['id']; ?>">

                                                            <select class="custom-select" name="teacher_id[]">
                                                <option value="" >Select Teacher</option>

                                                <?php
                                                    while($teacher = mysqli_fetch_array($teachers)) {

                                                        ?>

                                                <option value="<?php echo $teacher['id']; ?>" <?php echo ($teacher['id'] == $res['teacher_id'])?'selected':''; ?> ><?php echo $teacher['name']; ?>
                                                    
                                                </option>
                                               <?php

                                           }

                                               $teachers = get_complete_reord('users','role','teacher');

                                               ?>
                                            </select>

                                        </td>
                                                        
                                                        <td>admin@mail.com</td>
                                                        <td><?php echo $res['program']; ?></td>
                                                        <td>
                                                            <h5 class="m-b-0"><?php echo $res['smester']; ?></h5>
                                                        </td>
                                                       
                                                    </tr>

                                                <?php } ?>
                                                    
                                                    
                                                    
                                                    
                                                </tbody>
                                            </table>

                                              <div class="form-group">
                                            <div class="col-sm-12">
                                                <button class="btn btn-success" name="submit">Add Super Visors</button>
                                            </div>
                                        </div>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- ============================================================== -->
                            <!-- Table -->
                            <!-- ============================================================== -->
                            <!-- ============================================================== -->
                            <!-- Recent comment and chats -->
                            <!-- ============================================================== -->
                            
                            <!-- ============================================================== -->
                            <!-- Recent comment and chats -->
                            <!-- ============================================================== -->
                        </div>
                        <!-- ============================================================== -->
                        <!-- End Container fluid  -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- footer -->
                        <!-- ============================================================== -->
                       
                        <!-- ============================================================== -->
                        <!-- End footer -->
                        <!-- ============================================================== -->
                    </div>
                    <!-- ============================================================== -->
                    <!-- End Page wrapper  -->
                    <!-- ============================================================== -->
                </div>
                <!-- ============================================================== -->
                <!-- End Wrapper -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- All Jquery -->
                <!-- ============================================================== -->
                <?php include_once('scripts.php');?>
            </body>

            </html>