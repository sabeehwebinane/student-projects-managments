<!DOCTYPE html>
<html dir="ltr" lang="en">

<?php include_once('heads.php'); ?>

<?php include_once('functions.php');?>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full" data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php include_once('header.php'); ?>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php

        $c_user_id  ='';

        if(isset($_GET['student_id'])){

            $c_user_id  = $_GET['student_id'];

            $student        = get_single_record('users','id',$c_user_id);

            $student_record = get_single_record('students','user_id',$c_user_id);

        }
        

        include_once('menus.php');



        ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row align-items-center">
                    <div class="col-5">
                        <h4 class="page-title">Student Proposal</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Proposal</li>
                                </ol>
                            </nav>
                        </div>
                    </div>

                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-12 col-xlg-12 col-md-12">
                        <div class="card">

                            <?php  if(!empty( $c_user_id)) :?>
                            <div class="card-body">
                                <center class="m-t-30"> 
                                    <h4 class="card-title m-t-10"><?php echo $student_record['name']; ?></h4>
                                    <h6 class="card-subtitle">

                                        <?php echo $student_record['project_name']; ?>
                                            
                                        </h6>
                                    
                                </center>
                            </div>
                            <div>
                                <hr> </div>
                                <div class="card-body"> 
                                    <h6>Email address</h6>
                                    <small class="text-muted"><?php echo $student['email']; ?> </small>

                                    <h6>Session</h6> 
                                    <small class="text-muted p-t-30 db"><?php echo $student_record['session']; ?></small>

                                    <h6>Smester</h6>
                                    <small class="text-muted p-t-30 db"><?php echo $student_record['smester']; ?></small>

                                    <h6>Program</h6>
                                    <small class="text-muted p-t-30 db"><?php echo $student_record['program']; ?></small>

                                    <h6>Project Domain</h6>
                                    <small class="text-muted p-t-30 db"><?php echo $student_record['project_domain']; ?></small>

                                    <h6>Project Scope</h6>
                                    <small class="text-muted p-t-30 db"><?php echo $student_record['project_scope']; ?></small>

                                    <h6>Project Description</h6>
                                    <small class="text-muted p-t-30 db"><?php echo $student_record['project_decription']; ?></small>

                                </div>
                                <?php else:?>

                                    <div class="alert alert-danger">You Cant Access This Page Directoly</div>

                                <?php endif; ?>
                            </div>
                        </div>
                        <!-- Column -->

                    </div>
                    <!-- Row -->
                    <!-- ============================================================== -->
                    <!-- End PAge Content -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- Right sidebar -->
                    <!-- ============================================================== -->
                    <!-- .right-sidebar -->
                    <!-- ============================================================== -->
                    <!-- End Right sidebar -->
                    <!-- ============================================================== -->
                </div>
                <!-- ============================================================== -->
                <!-- End Container fluid  -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- footer -->
                <!-- ============================================================== -->
                <footer class="footer text-center">
                    All Rights Reserved by Xtreme Admin. Designed and Developed by <a href="https://wrappixel.com">WrapPixel</a>.
                </footer>
                <!-- ============================================================== -->
                <!-- End footer -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Page wrapper  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Wrapper -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- All Jquery -->
        <!-- ============================================================== -->
        <?php include_once('scripts.php');?>
    </body>

    </html>