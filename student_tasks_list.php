<!DOCTYPE html>
<html dir="ltr" lang="en">

<?php include_once('functions.php');?>

<?php include_once('heads.php'); ?>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full" data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php include_once('header.php'); ?>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php include_once('menus.php'); ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->

        <?php
        $commentor_id = $_SESSION['username'];

        $student_id   = '';

        if( isset($_GET['student_id'])){

           $commentor_id = $_SESSION['username'];

           $student_id = $_GET['student_id'];

       }

       $comments   = get_complete_reord('feedback','student_id',$student_id);

       $result = '';

       if (isset($_REQUEST['submit'])){

        // removes backslashes
        $message      = stripslashes($_REQUEST['message']);

        //escapes special characters in a string
        $message      = mysqli_real_escape_string($db,$message);

        $commentor_id = stripslashes($_REQUEST['commentor_id']);

        $commentor_id = mysqli_real_escape_string($db,$commentor_id);

        $student_id   = stripslashes($_REQUEST['student_id']);

        $student_id   = mysqli_real_escape_string($db,$student_id);

        $query    = "INSERT into feedback (user_id, description, student_id)
        VALUES ('$commentor_id', '$message', '$student_id')";  

        $result = mysqli_query($db,$query);

    }


    ?>
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="page-breadcrumb">
            <div class="row align-items-center">
                <div class="col-5">
                    <h4 class="page-title">Tasks List</h4>
                    <div class="d-flex align-items-center">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Tasks</li>
                            </ol>
                        </nav>
                    </div>
                </div>

            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Sales chart -->
            <!-- ============================================================== -->
            <div class="row">


                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- column -->
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Task List Report</h4>
                            </div>

                            <?php

                            if(!isset($_SESSION['username']) ||  !isset($_GET['student_id'])){?>

                                <div class="alert alert-danger">

                                    You Cant Access This Page Directly
                                </div>

                                <?php


                            }

                            else{

                                ?>
                                <div class="comment-widgets scrollable">
                                    <!-- Comment Row -->



                                    <?php

                                    if($result)

                                    echo '<h2 class="alert alert-success">Message Has Been Sent</h2>';
                                    while($comment = mysqli_fetch_array($comments)) {


                                        ?>
                                        <div class="d-flex flex-row comment-row m-t-0">
                                            <div class="p-2"><img src="assets/images/users/1.jpg" alt="user" width="50" class="rounded-circle"></div>
                                            <div class="comment-text w-100">
                                                <h6 class="font-medium">
                                                    <?php

                                                    $user = get_single_record('users','id',$comment['user_id']);

                                                    if($user)

                                                    {
                                                        echo $user['name'].' ( '.$user['role'].' )'; 
                                                    }
                                                        ?>

                                                </h6>
                                                <span class="m-b-15 d-block">

                                                    <?php echo $comment['description']; ?> </span>

                                                </div>
                                            </div>

                                        <?php } ?>

                                    </div>

                                    <form class="form-horizontal form-material" method="post" action="">

                                        <div class="form-group">

                                            <input type="hidden" name="commentor_id" value="<?php echo  $commentor_id; ?>">

                                            <input type="hidden" name="student_id" value="<?php echo  $student_id; ?>">

                                            <label class="col-md-12">Message</label>
                                            <div class="col-md-12">
                                                <textarea rows="5" class="form-control form-control-line" name="message"></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <button class="btn btn-success" name="submit">Send Message</button>
                                                </div>
                                            </div>

                                        </form>

                                    <?php }?>
                                </div>
                            </div>
                            <!-- column -->

                        </div>
                        <!-- ============================================================== -->
                        <!-- Recent comment and chats -->
                        <!-- ============================================================== -->
                    </div>
                    <!-- ============================================================== -->
                    <!-- End Container fluid  -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- footer -->
                    <!-- ============================================================== -->

                    <!-- ============================================================== -->
                    <!-- End footer -->
                    <!-- ============================================================== -->
                </div>
                <!-- ============================================================== -->
                <!-- End Page wrapper  -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Wrapper -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- All Jquery -->
            <!-- ============================================================== -->
            <?php include_once('scripts.php');?>
        </body>

        </html>