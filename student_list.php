<!DOCTYPE html>
<html dir="ltr" lang="en">

<?php include_once('functions.php');?>

<?php include_once('heads.php'); ?>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full" data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php include_once('header.php'); ?>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php include_once('menus.php'); ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->

        <?php
        $query = "SELECT * FROM students";
        $result = mysqli_query($db, $query);


        ?>
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row align-items-center">
                    <div class="col-5">
                        <h4 class="page-title">Students List</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Library</li>
                                </ol>
                            </nav>
                        </div>
                    </div>

                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales chart -->
                <!-- ============================================================== -->
                <div class="row">


                    <!-- ============================================================== -->
                    <!-- Table -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <!-- column -->
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <!-- title -->
                                    <div class="d-md-flex align-items-center">
                                        <div>
                                            <h4 class="card-title">Student List With Project Status</h4>

                                        </div>

                                    </div>
                                    <!-- title -->
                                </div>
                                <div class="table-responsive">
                                    <table class="table v-middle">
                                        <thead>
                                            <tr class="bg-light">
                                                <th class="border-top-0">Name</th>
                                                <th class="border-top-0">Project</th>
                                                <th class="border-top-0">Teacher</th>

                                                <th class="border-top-0">Email</th>
                                                <th class="border-top-0">Program</th>
                                                <th class="border-top-0">Smaster</th>
                                                <th class="border-top-0">Status</th>
                                                <th class="border-top-0"> Download Documentation</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <?php
                                            while($res = mysqli_fetch_array($result)) {

                                                ?>
                                                <tr>
                                                    <td>
                                                        <div class="d-flex align-items-center">

                                                            <div class="">
                                                                <h4 class="m-b-0 font-16"><?php echo $res['name']; ?></h4>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td><?php echo $res['project_name']; ?></td>
                                                    <td>

                                                        <?php

                                                        $teacher = get_single_record('users','id',$res['teacher_id']);

                                                        echo $teacher['name'];
                                                        ?>

                                                    </td>

                                                    <td>

                                                        <?php

                                                        $student = get_single_record('users','id',$res['user_id']);

                                                        echo $student['email'];
                                                        ?>

                                                    </td>
                                                    <td><?php echo $res['program']; ?></td>
                                                    <td>
                                                        <h5 class="m-b-0"><?php echo $res['smester']; ?></h5>
                                                    </td>
                                                    <td>

                                                        <?php if($res['project_status'] == 'approved'): ?>
                                                            <label class="label label-success">Approved</label>
                                                            <?php elseif($res['project_status'] == 'rejected'): ?>

                                                                <label class="label label-danger">Rejected</label>
                                                                <?php else: ?>

                                                                   <label class="label label-info">Pending</label>

                                                               <?php endif; ?>
                                                           </td>

                                                           <td>
                                                            <a href="files/<?php echo $res['file_path']; ?>" download>Download File</a>
                                                        </td>
                                                    </tr>

                                                    <?php
                                                }
                                                ?>



                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- ============================================================== -->
                        <!-- Table -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- Recent comment and chats -->
                        <!-- ============================================================== -->

                        <!-- ============================================================== -->
                        <!-- Recent comment and chats -->
                        <!-- ============================================================== -->
                    </div>
                    <!-- ============================================================== -->
                    <!-- End Container fluid  -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- footer -->
                    <!-- ============================================================== -->

                    <!-- ============================================================== -->
                    <!-- End footer -->
                    <!-- ============================================================== -->
                </div>
                <!-- ============================================================== -->
                <!-- End Page wrapper  -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Wrapper -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- All Jquery -->
            <!-- ============================================================== -->
            <?php include_once('scripts.php');?>
        </body>

        </html>