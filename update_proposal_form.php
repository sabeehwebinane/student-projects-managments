<!DOCTYPE html>
<html dir="ltr" lang="en">

<?php include_once('heads.php'); ?>

<?php include_once('functions.php');?>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full" data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php include_once('header.php'); ?>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php

        // If form submitted, insert values into the database.
        
        $student_id     = $_SESSION['username'];

        $student_record =    get_single_record('students','user_id',$student_id);

        $updated = false;

        $error   = false;



        if (isset($_REQUEST['submit'])){

         $username           = stripslashes($_REQUEST['name']);

        //escapes special characters in a string
         $username           = mysqli_real_escape_string($db,$username);

         $project_name       = stripslashes($_REQUEST['project_name']);

         $reg_no             = stripslashes($_REQUEST['reg_no']);

         $program            = stripslashes($_REQUEST['program']);

         $session            = stripslashes($_REQUEST['session']);

         $smester            = stripslashes($_REQUEST['smester']);

         $project_decription = stripslashes($_REQUEST['project_decription']);

         $project_scope      = stripslashes($_REQUEST['project_scope']);

         $project_domain     = stripslashes($_REQUEST['project_domain']);

         if(empty($username) || empty($project_decription) || empty($project_domain) || empty($project_scope)){

            $error = true;

        }

        if(!$error ){

            $result = mysqli_query($db, "UPDATE students SET name='$username' , project_name='$project_name', reg_no='$reg_no', program='$program', session='$session', smester='$smester', project_decription='$project_decription', project_scope='$project_scope', project_domain='$project_domain'  WHERE user_id=$student_id");

            $updated = true;

        }  

        


    }


    include_once('menus.php');



    ?>
    <!-- ============================================================== -->
    <!-- End Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Page wrapper  -->
    <!-- ============================================================== -->
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="page-breadcrumb">
            <div class="row align-items-center">
                <div class="col-5">
                    <h4 class="page-title">Proposal Form</h4>
                    <div class="d-flex align-items-center">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Proposal Form Update</li>
                            </ol>
                        </nav>
                    </div>
                </div>

            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->
            <!-- Row -->
            <div class="row">
                <!-- Column -->
                <div class="col-lg-12 col-xlg-12 col-md-12">
                    <div class="card">

                       <div class="card-body">
                        <?php

                        if($error ):

                            echo '<h4 class="alert alert-danger">Please Fill All The Fields</h4>';

                        endif;

                        if($updated ):

                            ?>

                            <h4 class="alert alert-success">Your Proposal has been updated</h4>

                            <?php

                        endif;

                        ?>
                        <form class="form-horizontal form-material" name="registration" action="" method="post" enctype="multipart/form-data">

                         <div class="form-group">
                            <label class="col-md-12">Full Name</label>
                            <div class="col-md-12">
                                <input type="text" placeholder="Johnathan Doe" class="form-control form-control-line" name="name" value="<?php echo $student_record['name']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">Registration Number</label>
                            <div class="col-md-12">
                                <input type="text" placeholder="227788" class="form-control form-control-line" name="reg_no" value="<?php echo $student_record['reg_no']; ?>">
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="example-email" class="col-md-12">Program</label>
                            <div class="col-md-12">
                                <input type="Text" placeholder="BS(Computer Science)" class="form-control form-control-line" name="program" id="example-email" value="<?php echo $student_record['program']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="example-email" class="col-md-12">Session</label>
                            <div class="col-md-12">
                                <input type="Text" placeholder="2k15" class="form-control form-control-line" id="example-email" name="session" value="<?php echo $student_record['session']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="example-email" class="col-md-12">Smester</label>
                            <div class="col-md-12">
                                <input type="Text" placeholder="4th" class="form-control form-control-line" id="example-email" name="smester" value="<?php echo $student_record['smester']; ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="example-email" class="col-md-12">Project Name</label>
                            <div class="col-md-12">
                                <input type="text" placeholder="Library Managment System" class="form-control form-control-line" name="project_name" id="example-email" value="<?php echo $student_record['project_name']; ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="example-email" class="col-md-12">Project Scope</label>
                            <div class="col-md-12">
                                <input type="text" placeholder="Your Project Scope" class="form-control form-control-line" name="project_scope" id="example-email" value="<?php echo $student_record['project_scope']; ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="example-email" class="col-md-12">Project Domain</label>
                            <div class="col-md-12">
                                <input type="text" placeholder="PHP" class="form-control form-control-line" name="project_domain" id="example-email" value="<?php echo $student_record['project_domain']; ?>">
                            </div>
                        </div>

                        <div class="form-group">

                            <label class="col-md-12">Project Description</label>
                            <div class="col-md-12">
                                <textarea rows="5" class="form-control form-control-line" name="project_decription"><?php echo $student_record['project_decription']; ?></textarea>
                            </div>
                        </div>



                        <div class="form-group">
                            <div class="col-sm-12">
                                <button class="btn btn-success" name="submit">Update Now</button>
                            </div>
                        </div>
                    </form>

                    
                </div>
            </div>
        </div>
        <!-- Column -->

    </div>
    <!-- Row -->
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Right sidebar -->
    <!-- ============================================================== -->
    <!-- .right-sidebar -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->

</div>
<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
<?php include_once('scripts.php');?>
</body>

</html>