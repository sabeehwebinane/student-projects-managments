<!DOCTYPE html>
<html dir="ltr" lang="en">

<?php include_once('heads.php'); ?>

<?php include_once('functions.php');?>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full" data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php include_once('header.php'); ?>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php

        // If form submitted, insert values into the database.
        
        $student_id     = $_SESSION['username'];

        $uploaded = false;



        if (isset($_REQUEST['submit'])){

         if(isset($_FILES['project_file']))
         {
           $file_name    = uniqid().$_FILES['project_file']['name'];

           $file_name    = str_replace(' ', '-', $file_name);

           $target       = "files/";

           $fileTarget   = $target.$file_name;

           $tempFileName = $_FILES["project_file"]["tmp_name"];

           $result       = move_uploaded_file($tempFileName,$fileTarget);

           $name         = stripslashes($_REQUEST['name']);

           $query        = "INSERT into instrutions (instruction_name, file_path )
           VALUES ('$name', '$file_name')"; 

           $result      = mysqli_query($db,$query);

           $uploaded    = true;


       }
       else{
        echo 'Please select your project documentation';
        exit;
    }  






}


include_once('menus.php');



?>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row align-items-center">
            <div class="col-5">
                <h4 class="page-title">Documentation Upload</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Upload Documentation</li>
                        </ol>
                    </nav>
                </div>
            </div>

        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <!-- Row -->
        <div class="row">
            <!-- Column -->
            <div class="col-lg-12 col-xlg-12 col-md-12">
                <div class="card">

                   <div class="card-body">
                    <?php 

                    if($uploaded):

                        ?>

                        <h4 class="alert alert-success">Your File Is Uploaded</h4>

                        <?php

                    endif;

                    ?>
                    <form class="form-horizontal form-material" name="registration" action="" method="post" enctype="multipart/form-data">

                      <div class="form-group">
                        <label class="col-md-12">Instructions Name</label>
                        <div class="col-md-12">
                            <input type="text" placeholder="Project Instructions BS(CS)" class="form-control form-control-line" name="name">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">Upload Instruction File</label>
                        <div class="col-md-12">

                            <input type="file" name="project_file" class="form-control form-control-line"> 
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-sm-12">
                            <button class="btn btn-success" name="submit">Upload Instruction</button>
                        </div>
                    </div>
                </form>


            </div>
        </div>
    </div>
    <!-- Column -->

</div>
<!-- Row -->
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Right sidebar -->
<!-- ============================================================== -->
<!-- .right-sidebar -->
<!-- ============================================================== -->
<!-- End Right sidebar -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- footer -->
<!-- ============================================================== -->
<footer class="footer text-center">
    All Rights Reserved by Xtreme Admin. Designed and Developed by <a href="https://wrappixel.com">WrapPixel</a>.
</footer>
<!-- ============================================================== -->
<!-- End footer -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
<?php include_once('scripts.php');?>
</body>

</html>