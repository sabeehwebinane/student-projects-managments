<!DOCTYPE html>
<html dir="ltr" lang="en">

<?php include_once('functions.php');?>

<?php

include_once('heads.php');

// If form submitted, insert values into the database.
$result    = '';

$st_result = '';

if (isset($_REQUEST['submit'])){



   /*if(isset($_FILES['project_file']))
   {
     $file_name = uniqid().$_FILES['project_file']['name'];

     $target = "files/";
     $fileTarget = $target.$file_name;
     $tempFileName = $_FILES["project_file"]["tmp_name"];

     $result = move_uploaded_file($tempFileName,$fileTarget);


 }
 else{
    echo 'Please select your project';
    exit;
}
*/

        // removes backslashes
$username           = stripslashes($_REQUEST['name']);

        //escapes special characters in a string
$username           = mysqli_real_escape_string($db,$username);

$email              = stripslashes($_REQUEST['email']);

$email              = mysqli_real_escape_string($db,$email);

$password           = stripslashes($_REQUEST['password']);

$password           = mysqli_real_escape_string($db,$password);

$project_name       = stripslashes($_REQUEST['project_name']);

$reg_no             = stripslashes($_REQUEST['reg_no']);

$program            = stripslashes($_REQUEST['program']);

$session            = stripslashes($_REQUEST['session']);

$smester            = stripslashes($_REQUEST['smester']);

$project_decription = stripslashes($_REQUEST['project_decription']);

$project_scope      = stripslashes($_REQUEST['project_scope']);

$project_domain     = stripslashes($_REQUEST['project_domain']);

if(empty($username) || empty($email) || empty($password)){
    echo 'Please Fill All The Fields';
    exit;
}

$query    = "INSERT into users (name, password, email,role)
VALUES ('$username', '$password', '$email','student')";  

$result = mysqli_query($db,$query);

if($result)
{
    $user_check_query = "SELECT * FROM users WHERE name='$username' OR email='$email' LIMIT 1";
    $result = mysqli_query($db, $user_check_query);
    $user = mysqli_fetch_assoc($result);
    $user_id = $user['id'];

    $st_query    = "INSERT into students (user_id, name, reg_no,program,project_name,session,smester,project_status,project_decription,project_scope,project_domain)
    VALUES ('$user_id','$username', '$reg_no', '$program','$project_name','$session','$smester','pending','$project_decription','$project_scope','$project_domain')";

    $st_result = mysqli_query($db,$st_query);


}

}



?>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full" data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper login-page">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row align-items-center">
                    <div class="col-5">
                        <h4 class="page-title">Student Proposal Form</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="login.php">Already Registered?</a></li>
                                    
                                </ol>
                            </nav>
                        </div>
                    </div>

                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">

                    <!-- Column -->

                    <div class="col-lg-8 col-xlg-12 col-md-7">
                        <div class="card">



                            <div class="card-body">

                               <?php if($st_result) : ?>

                                <div class="alert alert-success"><h3>You are registered successfully.</h3></div>

                            <?php endif; ?>
                            <form class="form-horizontal form-material" name="registration" action="" method="post" enctype="multipart/form-data">

                               <div class="form-group">
                                <label class="col-md-12">Full Name</label>
                                <div class="col-md-12">
                                    <input type="text" placeholder="Johnathan Doe" class="form-control form-control-line" name="name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Registration Number</label>
                                <div class="col-md-12">
                                    <input type="text" placeholder="227788" class="form-control form-control-line" name="reg_no">
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="example-email" class="col-md-12">Program</label>
                                <div class="col-md-12">
                                    <input type="Text" placeholder="BS(Computer Science)" class="form-control form-control-line" name="program" id="example-email">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="example-email" class="col-md-12">Session</label>
                                <div class="col-md-12">
                                    <input type="Text" placeholder="2k15" class="form-control form-control-line"  id="example-email" name="session">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="example-email" class="col-md-12">Smester</label>
                                <div class="col-md-12">
                                    <input type="Text" placeholder="4th" class="form-control form-control-line"  id="example-email" name="smester">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="example-email" class="col-md-12">Email</label>
                                <div class="col-md-12">
                                    <input type="email" placeholder="johnathan@admin.com" class="form-control form-control-line" name="email" id="example-email">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Password</label>
                                <div class="col-md-12">
                                    <input type="password" name="password" class="form-control form-control-line">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="example-email" class="col-md-12">Project Name</label>
                                <div class="col-md-12">
                                    <input type="text" placeholder="Library Managment System" class="form-control form-control-line" name="project_name" id="example-email">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="example-email" class="col-md-12">Project Scope</label>
                                <div class="col-md-12">
                                    <input type="text" placeholder="Your Project Scope" class="form-control form-control-line" name="project_scope" id="example-email">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="example-email" class="col-md-12">Project Domain</label>
                                <div class="col-md-12">
                                    <input type="text" placeholder="PHP" class="form-control form-control-line" name="project_domain" id="example-email">
                                </div>
                            </div>

                            <div class="form-group">

                                <label class="col-md-12">Project Description</label>
                                <div class="col-md-12">
                                    <textarea rows="5" class="form-control form-control-line" name="project_decription"></textarea>
                                </div>
                            </div>



                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button class="btn btn-success" name="submit">Register Now</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            <!-- Column -->
        </div>
        <!-- Row -->
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
<?php include_once('scripts.php');?>
</body>

</html>